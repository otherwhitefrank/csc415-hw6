/*  Name: Frank Dye
    Date: 4/16/2014
    Description: A multi-threaded word count program using posix
 */


#include <stdlib.h>
#include <stdio.h>
#include <time.h>

//Stupid C90 boolean definition
typedef int bool;
#define TRUE  1
#define FALSE 0

#define FRAME_TABLE_MAX_SIZE 16
#define MAX_PAGES 16
#define NUM_REQUESTS 100


//Forward Declarations
int frame_table_contains(int* frame_table, int num_frames, int val);
int get_next_free_frame(int* frame_table, int num_frames);
int get_oldest_frame(time_t* frame_table_times, int num_frames);

int main(int argc, char** argv)
{

    //Initialize index variables
    int i = 0;
    int j = 0;

    //Initialize random number generator
    time_t t;
    srand((unsigned) time(&t));

    //Create frame_table, num_frames changes over algorithm iteration
    int* frame_table;
    int num_frames = 1;


    //FIFO ALGORITHM
    
    printf("FIFO Algorithm\n");
    printf("CSV output: ");
    
    for (num_frames = 1; num_frames <= FRAME_TABLE_MAX_SIZE; num_frames++)
    {

        //Create table of page requests
        int page_requests[NUM_REQUESTS];

        //Zero table
        for (i = 0; i < NUM_REQUESTS; i++)
        {
            page_requests[i] = 0;
        }

        //Used to remember last random number generated so that we don't get duplicate numbers
        int last_val = -1;

        //Generate NUM_REQUESTS of non-repeating page requests 
        i = 0;
        while (i < NUM_REQUESTS)
        {
            int num = rand() % MAX_PAGES;
            if (last_val != num)
            {
                //New number, save it and increment i
                page_requests[i] = num;
                last_val = num;
                i++;
            }
        }

        
        
        

        //FIFO Page Algorithm
        int page_faults = 0;

        //Allocate frame_table
        frame_table = malloc(num_frames * sizeof (int));
        //Keep track of the least used page_table
        time_t* frame_swap_time = malloc(num_frames * sizeof (time_t));

        //Initialize each entry in frame_table to -1 to indicate it is unloaded
        for (i = 0; i < num_frames; i++)
        {
            frame_table[i] = -1;
        }


        for (j = 0; j < NUM_REQUESTS; j++)
        {
            //printf("i: %i \n", j);
            //Store current request
            int curr_request = page_requests[j];

            int frame_found_index = frame_table_contains(frame_table, num_frames, curr_request);
            //First check to see if the page table already contains the requests
            if (frame_found_index != -1)
            {
                ; //page found, do nothing in FIFO algorithm
            }
            else
            {
                //Page not found, cycle one out
                //Check to see if there is a free slot in the page_table
                int free_slot = get_next_free_frame(frame_table, num_frames);
                if (free_slot != -1)
                {
                    //Free slot found, bring in page table and increate page_fault count
                    frame_swap_time[free_slot] = time(NULL);
                    //printf("page empty: %i filled with request: %i \n", free_slot, curr_request);
                    frame_table[free_slot] = curr_request;
                    page_faults++;
                }
                else
                {
                    //No free slot so find the oldest slot and swap it out with the new request
                    int frame_to_swap = get_oldest_frame(frame_swap_time, num_frames);
                    //printf("page_to_swap: %i \n", frame_to_swap);

                    frame_table[frame_to_swap] = curr_request;
                    frame_swap_time[frame_to_swap] = time(NULL);
                    page_faults++;
                }

            }
        }
        //Pretty output
        //printf("Frames: %i Page Faults %i \n", num_frames, page_faults);
        //Comma seperated value output
        printf("%i,%i", num_frames, page_faults);
        if (num_frames != (FRAME_TABLE_MAX_SIZE))
        {
            printf(",");
        }
        
        
        free(frame_table);
        free(frame_swap_time);
        
    }
    

    printf("\n\n");
    
    //LRU ALGORITHM
    
    printf("LRU Algorithm\n");
    printf("CSV output: ");

    for (num_frames = 1; num_frames <= FRAME_TABLE_MAX_SIZE; num_frames++)
    {

        //Create table of page requests
        int page_requests[NUM_REQUESTS];

        //Zero table
        for (i = 0; i < NUM_REQUESTS; i++)
        {
            page_requests[i] = 0;
        }

        //Used to remember last random number generated so that we don't get duplicate numbers
        int last_val = -1;

        //Generate NUM_REQUESTS of non-repeating page requests 
        i = 0;
        while (i < NUM_REQUESTS)
        {
            int num = rand() % MAX_PAGES;
            if (last_val != num)
            {
                //New number, save it and increment i
                page_requests[i] = num;
                last_val = num;
                i++;
            }
        }

        
        
        //FIFO Page Algorithm
        int page_faults = 0;

        //Allocate frame_table
        frame_table = malloc(num_frames * sizeof (int));
        //Keep track of the least used page_table
        time_t* frame_swap_time = malloc(num_frames * sizeof (time_t));

        //Initialize each entry in frame_table to -1 to indicate it is unloaded
        for (i = 0; i < num_frames; i++)
        {
            frame_table[i] = -1;
        }


        for (j = 0; j < NUM_REQUESTS; j++)
        {
            //printf("i: %i \n", j);
            //Store current request
            int curr_request = page_requests[j];

            int frame_found_index = frame_table_contains(frame_table, num_frames, curr_request);
            //First check to see if the page table already contains the requests
            if (frame_found_index != -1)
            {
                frame_swap_time[frame_found_index] = time(NULL); //page found, touch the index's time
                                                                 //to indicate access to LRU algorithm
            }
            else
            {
                //Page not found, cycle one out
                //Check to see if there is a free slot in the page_table
                int free_slot = get_next_free_frame(frame_table, num_frames);
                if (free_slot != -1)
                {
                    //Free slot found, bring in page table and increate page_fault count
                    frame_swap_time[free_slot] = time(NULL);
                    //printf("page empty: %i filled with request: %i \n", free_slot, curr_request);
                    frame_table[free_slot] = curr_request;
                    page_faults++;
                }
                else
                {
                    //No free slot so find the oldest slot and swap it out with the new request
                    int frame_to_swap = get_oldest_frame(frame_swap_time, num_frames);
                    //printf("page_to_swap: %i \n", frame_to_swap);

                    frame_table[frame_to_swap] = curr_request;
                    frame_swap_time[frame_to_swap] = time(NULL);
                    page_faults++;
                }

            }
        }
        
        //Pretty output
        //printf("Frames: %i Page Faults %i \n", num_frames, page_faults);
        //Comma seperated value output
        printf("%i,%i", num_frames, page_faults);
        if (num_frames != (FRAME_TABLE_MAX_SIZE))
        {
            printf(",");
        }
        
        free(frame_table);
        free(frame_swap_time);
        
    }
    printf("\n");
    
    
    return 0;
}

//Check page_table to 

int frame_table_contains(int* frame_table, int num_frames, int val)
{
    int i = 0;
    int valFound = -1; //-1 indicates not found

    for (i = 0; i < num_frames; i++)
    {
        if (frame_table[i] == val)
        {
            //Found, set valFound to index of slot its at
            valFound = i;
            return valFound;
        }
    }

    return valFound;
}

//Get next free slot

int get_next_free_frame(int* frame_table, int num_frames)
{
    //-1 indicates a free slot in our page_table, check to see if we have any
    //returning that index, or -1 if we can't find a free slot
    int i = 0;

    int free_slot = -1;

    for (i = 0; i < num_frames; i++)
    {
        if (frame_table[i] == -1)
        {
            //Free slot found
            free_slot = i;
            return free_slot;
        }
    }

    return free_slot;

}

//Get the oldest value

int get_oldest_frame(time_t* frame_table_times, int num_frames)
{
    int i = 0; //Index variable

    int swapIndex = 0;
    for (i = 0; i < num_frames; i++)
    {
        if (frame_table_times[i] < frame_table_times[swapIndex])
        {
            swapIndex = i;
        }
    }
    
    return swapIndex;
}