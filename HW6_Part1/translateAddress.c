/*  Name: Frank Dye
    Date: 4/16/2014
    Description: A multi-threaded word count program using posix
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>


#define PAGE_SIZE 4096

/*
 * 
 */
int main(int argc, char** argv) {
    
    int inputAddress = 0;
    
    //Check for correct number of arguments
    if (argc != 2)
    {
        printf("Incorrect number of parameters to translateAddress!\n");
        exit(1);
    }

    if (isdigit(argv[1][0]))
    {   //String seems to be integer values so perform conversion.
        inputAddress = atoi(argv[1]);
    }
    else
    {
        printf("Entered parameter does not seem to be integer values!\n");
        exit(1);
    }
    
    printf("page number: %d\n", (inputAddress / PAGE_SIZE));
    printf("offset: %d\n", (inputAddress % PAGE_SIZE));
    
    return 0;
}

